package com.uet.english

import com.uet.english.core.repository.HomeRepository
import com.uet.english.main.home.viewmodel.HomeViewModel

object Injection {
    fun provideHomeViewModel(): HomeViewModel {
        val dao = App.shared().appDatabase.getListWordDao()
        val repository = HomeRepository(dao)
        return HomeViewModel(repository)
    }
}