package com.uet.english

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.room.Room
import com.uet.english.core.rest.Constants
import com.uet.english.data.local.AppDatabase
import com.uet.english.extensions.getColor
import com.uet.english.extensions.getString
import com.uet.english.utils.AppConstant
import com.uet.english.utils.HMPreference
import com.uet.english.utils.LogUtils

class App : Application(), LifecycleObserver, Application.ActivityLifecycleCallbacks {
    companion object {
        private lateinit var instance: App
        fun shared(): App {
            return instance
        }
    }

    var wasInBackground = false

    override fun onCreate() {
        super.onCreate()
        instance = this
        //First init
        Constants.API_BASE_PATH =  HMPreference.getString(
            AppConstant.BASE_URL,
            Constants.DEFAULT_SERVER
        )
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        registerActivityLifecycleCallbacks(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        // app moved to foreground
        wasInBackground = true
        LogUtils.d("App in foreground")

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        // app moved to background
        wasInBackground = false
        LogUtils.d("App in background")
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {

    }

    override fun onActivityStarted(p0: Activity) {

    }

    override fun onActivityResumed(p0: Activity) {

    }

    override fun onActivityPaused(p0: Activity) {

    }

    override fun onActivityStopped(p0: Activity) {

    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

    }

    override fun onActivityDestroyed(p0: Activity) {

    }

    var appDatabase: AppDatabase
        get() {
            return Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "uet_db"
            )
                .fallbackToDestructiveMigration().allowMainThreadQueries().build()
        }
        set(value) {}
}

enum class NotificationType { INFO, SUCCESS, ERROR }

//Global function
fun showNotificationBanner(type: NotificationType, message: String) {
    val layout: View = LayoutInflater.from(App.shared()).inflate(R.layout.banner, null)
    val title = layout.findViewById(R.id.banner_name) as TextView
    val content = layout.findViewById(R.id.banner_text) as TextView
    val background = layout.findViewById(R.id.customToast) as RelativeLayout
    val image = layout.findViewById(R.id.banner_profile) as ImageView
    content.text = message
    when (type) {
        NotificationType.INFO -> {
            title.text = R.string.str_information.getString()
            background.setBackgroundColor(R.color.colorPrimary.getColor())
            image.setImageResource(R.drawable.icon_info)
        }
        NotificationType.SUCCESS -> {
            title.text = R.string.str_success.getString()
            background.setBackgroundColor(R.color.color_tag_confirm.getColor())
            image.setImageResource(R.drawable.icon_confirm)
        }
        NotificationType.ERROR -> {
            title.text = R.string.str_error.getString()
            background.setBackgroundColor(R.color.color_unconfirm.getColor())
            image.setImageResource(R.drawable.icon_error)
        }
    }

    val toast = Toast(App.shared())
    toast.setGravity(Gravity.FILL_HORIZONTAL or Gravity.TOP, 0, 0)
    toast.duration = Toast.LENGTH_SHORT
    toast.view = layout
    toast.show()
}