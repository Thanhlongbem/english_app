package com.uet.english.base

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import com.uet.english.extensions.setSafeOnClickListener

@SuppressLint("NotifyDataSetChanged")
abstract class BaseAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data: MutableList<T> = ArrayList()
    var onClickListener: ((position: Int) -> Unit)? = null
    var deleteFunction: ((position: Int)-> Unit)? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Binder<T>).bind(data[position], position)
        holder.itemView.setSafeOnClickListener { onClickListener?.invoke(position) }
    }

    override fun getItemCount(): Int {
        return data.size
    }


    open fun addData(list: List<T>) {
        data.addAll(list)
        notifyDataSetChanged()
    }

    open fun updateData(list: List<T>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }


    open fun getItem(position: Int): T{
        return data[position]
    }

    fun removeItem(position: Int) {
        data.removeAt(position)
        notifyDataSetChanged()
    }

    fun removeAll() {
        data.clear()
        notifyDataSetChanged()
    }

    internal interface Binder<T> {
        fun bind(item: T, position: Int)
    }
}