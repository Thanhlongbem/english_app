package com.uet.english.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.uet.english.R
import com.uet.english.comon.model.LoadingDialog
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity : AppCompatActivity(), View.OnClickListener {
    val compositeDisposable = CompositeDisposable()
    private var loadingDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        setContentView(setLayoutId())
        initView()
    }

    abstract fun setLayoutId(): View

    abstract fun initView()

    override fun onClick(view: View?) {
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }




    /*-------*/
    fun setOnClickListener(vararg ids: View) {
        for (view in ids) view.setOnClickListener(this)
    }

    fun showLoading(show: Boolean) {
        if(loadingDialog == null){
            loadingDialog = LoadingDialog(this)
        }
        if (show) {
            loadingDialog?.show()
        } else {
            if (loadingDialog?.isShowing == true) loadingDialog?.dismiss()
        }
    }
}