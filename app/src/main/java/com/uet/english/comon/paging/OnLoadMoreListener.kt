package com.uet.english.comon.paging

interface OnLoadMoreListener {
    fun onLoadMore()
}