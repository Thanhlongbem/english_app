package com.uet.english.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.res.Resources
import android.graphics.Typeface
import android.net.Uri
import android.os.SystemClock
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Dimension
import androidx.fragment.app.Fragment
import com.uet.english.App
import com.uet.english.R
import com.uet.english.core.rest.Constants
import com.google.gson.Gson
import com.jakewharton.rxbinding3.view.clicks
import com.uet.english.utils.LogUtils
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.concurrent.TimeUnit

fun Context.toActivity(): Activity? {
    var context = this
    while (context is ContextWrapper) {
        if (context is Activity) {
            return context
        }
        context = context.baseContext
    }
    return null
}

@JvmOverloads @Dimension(unit = Dimension.PX) fun Number.dpToPx(
    metrics: DisplayMetrics = Resources.getSystem().displayMetrics
): Float {
    return toFloat() * metrics.density
}

@JvmOverloads @Dimension(unit = Dimension.DP) fun Number.pxToDp(
    metrics: DisplayMetrics = Resources.getSystem().displayMetrics
): Float {
    return toFloat() / metrics.density
}

fun Disposable.disposedBy(compositeDisposable: CompositeDisposable){
    compositeDisposable.add(this)
}


fun String.toast(context: Context = App.shared(), length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(context, this, length).show()
}

inline fun <reified T : View> Activity.find(id: Int): T = findViewById<T>(id)

fun <T> Single<T>.applyOn(): Single<T> {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun String?.ifEmptyLetBe(alt: String): String {
    if (this == null){
        return alt
    }
    if (this.trim().isEmpty()) {
        return alt
    }
    return this
}

fun Any.toJsonString(): String = Gson().toJson(this)

inline fun <T1 : Any, T2 : Any, R : Any> ifLet(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}

inline fun <T1 : Any, T2 : Any, T3 : Any, R : Any> ifLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    block: (T1, T2, T3) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null) block(p1, p2, p3) else null
}

fun <T> Single<T>.execute(tag: String = ""): Disposable{
    return this.subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe({
            if (tag.isNotBlank()) LogUtils.d("$tag Task success")
        }, {
            if (tag.isNotBlank()) LogUtils.d("$tag Task error")
        })
}

fun isContainsSpecialCharacter(input: String): Boolean {
    val regex = Regex("[^A-Za-z0-9_ ]+")
    return regex.containsMatchIn(input)
}


fun Double.format(): String{
    val valueAsBD: BigDecimal = BigDecimal.valueOf(this)
    valueAsBD.setScale(2, BigDecimal.ROUND_HALF_UP)
    val custom = DecimalFormatSymbols()
    custom.decimalSeparator = '.'
    custom.groupingSeparator = ','
    custom.currencySymbol = ""
    val format: DecimalFormat = DecimalFormat.getInstance() as DecimalFormat
    format.decimalFormatSymbols = custom
    return try {format.format(valueAsBD)} catch (e: Exception) {return "$this"}
}

fun Int.format(): String{
    val custom = DecimalFormatSymbols()
    custom.decimalSeparator = '.'
    custom.groupingSeparator = ','
    custom.currencySymbol = ""
    val format: DecimalFormat = DecimalFormat.getInstance() as DecimalFormat
    format.decimalFormatSymbols = custom
    return try {format.format(this)} catch (e: Exception) {return "$this"}
}

fun Float.format(): String{
    val custom = DecimalFormatSymbols()
    custom.decimalSeparator = '.'
    custom.groupingSeparator = ','
    custom.currencySymbol = ""
    val format: DecimalFormat = DecimalFormat.getInstance() as DecimalFormat
    format.decimalFormatSymbols = custom
    return try {format.format(this)} catch (e: Exception) {return "$this"}
}

fun String.toDoubleValue(): Double{
    val custom = DecimalFormatSymbols()
    custom.decimalSeparator = '.'
    custom.groupingSeparator = ','
    custom.currencySymbol = ""
    val format: DecimalFormat = DecimalFormat.getInstance() as DecimalFormat
    format.decimalFormatSymbols = custom
    return try {format.parse(this).toDouble()} catch (e: Exception) {return 0.0}
}

fun String.toRequestBody(): RequestBody {
    return this.toRequestBody("text/plain".toMediaTypeOrNull())
}


fun Context.setClipboard(v: String): Boolean {
    val myClipboard= getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val myClip: ClipData = ClipData.newPlainText(Constants.CLIPBOARD, v)
    myClipboard.setPrimaryClip(myClip)
    "Copied".toast(this)
    return true
}


@SuppressLint("CheckResult")
fun View.setSafeOnClickListener(onClick: (View) -> Unit) {
    this.clicks().throttleFirst(500, TimeUnit.MILLISECONDS).subscribe {
        onClick(this)
    }
}


private var lastClickedTime = 0L
fun View.setSafeClickListener(onViewClick: View.OnClickListener) {
    this.setOnClickListener {
        if (SystemClock.elapsedRealtime() - lastClickedTime > 500){
            lastClickedTime = SystemClock.elapsedRealtime()
            onViewClick.onClick(it)
        }
    }
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

