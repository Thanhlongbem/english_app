package com.uet.english.extensions

import android.app.Activity
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment

fun NavController.safeNavigateWithoutAnimation(direction: NavDirections, args: Bundle? = null) {
    currentDestination?.getAction(direction.actionId)?.run {
        if (args != null) {
            navigate(direction.actionId, args)
        } else {
            navigate(direction)
        }
    }
}

fun <T : NavHostFragment> T.initNavGraph(
    @NavigationRes navGraph: Int,
    @IdRes startDestination: Int,
    bundle: Bundle? = Bundle()
) {
    val inflater = navController.navInflater
    val graph = inflater.inflate(navGraph)
    graph.setStartDestination(startDestination)
    navController.setGraph(graph, bundle)
}

fun <T : Activity> T.hasArgs(): Boolean {
    return intent != null && intent.extras != null
}

fun <T : AppCompatActivity> T.navHostFragment(hostFragmentID: Int): NavHostFragment? {
    return supportFragmentManager.findFragmentById(hostFragmentID) as NavHostFragment?
}
