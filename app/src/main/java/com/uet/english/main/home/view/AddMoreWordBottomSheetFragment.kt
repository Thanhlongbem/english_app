package com.uet.english.main.home.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.uet.english.R
import com.uet.english.data.entities.ListWordEntity
import com.uet.english.databinding.DialogAddWordBinding
import com.uet.english.extensions.isContainsSpecialCharacter

class AddMoreWordBottomSheetFragment() : BottomSheetDialogFragment() {
    var onAddWordClick: ((word: ListWordEntity) -> Unit)? = null
    private var _binding: DialogAddWordBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val bottomSheet = bottomSheetDialog
                .findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)

            if (bottomSheet != null) {
                val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(bottomSheet)
                behavior.isDraggable = true
            }
        }
        return bottomSheetDialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogAddWordBinding.inflate(inflater, container, false)
        dialog?.window?.decorView?.setBackgroundResource(android.R.color.transparent)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAdd.setOnClickListener {
            if (
                binding.edtEnglish.text.isNullOrEmpty() ||
                binding.edtVietnamese.text.isNullOrEmpty() ||
                isContainsSpecialCharacter(binding.edtEnglish.text.toString())
            ) {
                Toast.makeText(requireContext(), "Wrong information", Toast.LENGTH_SHORT).show()
            } else {
                onAddWordClick?.invoke(ListWordEntity(binding.edtEnglish.text.toString(), binding.edtVietnamese.text.toString()))
                dialog?.dismiss()
            }
        }
    }
}