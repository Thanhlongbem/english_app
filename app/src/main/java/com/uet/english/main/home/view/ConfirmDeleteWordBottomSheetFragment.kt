package com.uet.english.main.home.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.uet.english.R
import com.uet.english.data.entities.ListWordEntity
import com.uet.english.databinding.DialogConfirmDeleteWordBinding

class ConfirmDeleteWordBottomSheetFragment(private val word: ListWordEntity) : BottomSheetDialogFragment() {
    var onDeleteWordClick: ((word: ListWordEntity) -> Unit)? = null
    private var _binding: DialogConfirmDeleteWordBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val bottomSheet = bottomSheetDialog
                .findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)

            if (bottomSheet != null) {
                val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(bottomSheet)
                behavior.isDraggable = true
            }
        }
        return bottomSheetDialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogConfirmDeleteWordBinding.inflate(inflater, container, false)
        dialog?.window?.decorView?.setBackgroundResource(android.R.color.transparent)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvWord.text = word.english

        binding.btnCancel.setOnClickListener {
            dialog?.dismiss()
        }

        binding.btnDelete.setOnClickListener {
            onDeleteWordClick?.invoke(word)
            dialog?.dismiss()
        }
    }
}