package com.uet.english.main.home.view

import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.widget.textChanges
import com.uet.english.Injection
import com.uet.english.R
import com.uet.english.base.BaseFragment
import com.uet.english.core.rest.Status
import com.uet.english.databinding.FragmentHomeBinding
import com.uet.english.extensions.disposedBy
import com.uet.english.extensions.getViewModel
import com.uet.english.extensions.hideKeyboard
import com.uet.english.extensions.viewBinding
import com.uet.english.main.home.adapter.WordAdapter
import com.uet.english.main.home.model.WordResponse
import com.uet.english.main.home.viewmodel.HomeViewModel
import com.uet.english.utils.FileUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class HomeFragment: BaseFragment() {
    private val viewModel: HomeViewModel by lazy {
        getViewModel { Injection.provideHomeViewModel() }
    }
    private lateinit var adapter: WordAdapter
    private lateinit var dialogShowWord: TranslatedTextBottomSheetFragment
    private lateinit var dialogAddMoreWord: AddMoreWordBottomSheetFragment
    private lateinit var dialogDeleteWord: ConfirmDeleteWordBottomSheetFragment
    private val binding by viewBinding(FragmentHomeBinding::bind)
    private var listAllWordLocal = mutableListOf<WordResponse>()
    private var listFilterResult = mutableListOf<WordResponse>()
    private var isFABOpen: Boolean = false


    override fun setLayoutId(): Int = R.layout.fragment_home

    override fun initView() {
        observerData()
        listFilterResult = listAllWordLocal
        adapter = WordAdapter()
        adapter.onWordClicklistener = {
            dialogShowWord = TranslatedTextBottomSheetFragment(it)
            dialogShowWord.show(requireActivity().supportFragmentManager, "")
        }
        adapter.onDeleteWordlistener = {
            dialogDeleteWord = ConfirmDeleteWordBottomSheetFragment(it.mapToEntity())
            dialogDeleteWord.onDeleteWordClick = {
                viewModel.deleteWord(it)
            }
            dialogDeleteWord.show(requireActivity().supportFragmentManager, "")
        }
        adapter.data = listAllWordLocal
        binding.rvListWord.layoutManager = LinearLayoutManager(requireContext())
        binding.rvListWord.adapter = adapter
        binding.rvListWord.setHasFixedSize(true)
        handleOnTextChangeToFilter()
        handleActionClick()
        hideFABMenu()
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
        viewModel.getAllWordInDB(requireContext())
    }

    private fun handleActionClick() {
        binding.btnAction.setOnClickListener {
            if (!isFABOpen) {
                showFABMenu()
            } else {
                hideFABMenu()
            }
        }

        binding.btnAddWord.setOnClickListener {
            dialogAddMoreWord = AddMoreWordBottomSheetFragment()
            dialogAddMoreWord.onAddWordClick = {
                viewModel.addWord(it)
            }
            dialogAddMoreWord.show(requireActivity().supportFragmentManager, "")
        }
    }

    private fun handleOnTextChangeToFilter() {
        binding.edtSearchText.textChanges()
            .skip(1)
            .map { it.toString() }
            .debounce(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ text ->
                if (!text.isNullOrEmpty()) {
                    listFilterResult = viewModel.searchWord(text, listAllWordLocal)
                    adapter.updateData(listFilterResult)
                } else {
                    adapter.updateData(listAllWordLocal)
                }
            }, {
                adapter.updateData(listFilterResult)
            }).disposedBy(compositeDisposable)
    }

    private fun observerData() {
        viewModel.getListWordLiveData.observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    showLoading(false)
                    listAllWordLocal = it.data ?: mutableListOf()
                    adapter.updateData(listAllWordLocal)
                }

                Status.LOADING -> {
                    showLoading(true)
                }

                Status.ERROR -> {
                    showLoading(false)
                }
            }
        }

        viewModel.addWordLiveData.observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    showLoading(false)
                    binding.edtSearchText.setText("")
                    viewModel.getAllWordInDB(requireContext())
                }

                Status.LOADING -> {
                    showLoading(true)
                }

                Status.ERROR -> {
                    showLoading(false)
                    Toast.makeText(requireContext(), "Duplicate word", Toast.LENGTH_SHORT).show()
                }
            }
        }

        viewModel.deleteWordLiveData.observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    showLoading(false)
                    binding.edtSearchText.setText("")
                    viewModel.getAllWordInDB(requireContext())
                }

                Status.LOADING -> {
                    showLoading(true)
                }

                Status.ERROR -> {
                    showLoading(false)
                    Toast.makeText(requireContext(), "Delete word fail", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun showFABMenu() {
        isFABOpen = true
        binding.btnAddWord.animate().translationY(-resources.getDimension(R.dimen.standard_75))
        binding.btnPlayGame.animate().translationY(-resources.getDimension(R.dimen.standard_145))
    }

    private fun hideFABMenu() {
        isFABOpen = false
        binding.btnAddWord.animate().translationY(0f)
        binding.btnPlayGame.animate().translationY(0f)
    }
}
