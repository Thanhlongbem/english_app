package com.uet.english.main.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.uet.english.base.BaseAdapter
import com.uet.english.databinding.ItemWordBinding
import com.uet.english.main.home.model.WordResponse

class WordAdapter: BaseAdapter<WordResponse>() {
    var onWordClicklistener: ((str: WordResponse) -> Unit)? = null
    var onDeleteWordlistener: ((str: WordResponse) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
            = WordViewHoler(ItemWordBinding.inflate(LayoutInflater.from(parent.context), parent, false))


    inner class WordViewHoler(val binding: ItemWordBinding) : RecyclerView.ViewHolder(binding.root), Binder<WordResponse> {
        override fun bind(item: WordResponse, position: Int) {
            binding.tvWord.text = item.english
            binding.tvWord.setOnClickListener {
                onWordClicklistener?.invoke(item)
            }

            binding.tvWord.setOnLongClickListener {
                onDeleteWordlistener?.invoke(item)
                true
            }
        }

    }
}