package com.uet.english.main.login.model

import com.google.gson.annotations.SerializedName

data class UserInfo (
    @SerializedName("UserId")
    val userId: String = ""
)