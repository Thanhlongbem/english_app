package com.uet.english.main.home.model

import com.uet.english.data.entities.ListWordEntity

data class WordResponse(
    val english: String = "",
    val vietnamese: String = ""
) {
    fun mapToEntity(): ListWordEntity {
        return ListWordEntity(english, vietnamese)
    }
}
