package com.uet.english.main.home.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.uet.english.R
import com.uet.english.base.BaseViewModel
import com.uet.english.core.repository.HomeRepository
import com.uet.english.core.rest.ResponseData
import com.uet.english.data.entities.ListWordEntity
import com.uet.english.extensions.applyOn
import com.uet.english.extensions.disposedBy
import com.uet.english.main.home.model.WordResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.InputStreamReader

class HomeViewModel(private val repository: HomeRepository): BaseViewModel() {

    val getListWordLiveData = MutableLiveData<ResponseData<MutableList<WordResponse>>>()
    val addWordLiveData = MutableLiveData<ResponseData<Boolean>>()
    val deleteWordLiveData = MutableLiveData<ResponseData<Boolean>>()

    fun getAllWordInDB(context: Context) {
        getListWordLiveData.postValue(ResponseData.loading())
        repository.getAllWord()
            .applyOn()
            .subscribe({
                val result = mutableListOf<WordResponse>()
                for (entity in it) {
                    result.add(entity.mapToModel())
                }
                if (result.isEmpty()) {
                    getListWordLiveData.postValue(ResponseData.success(getWordsFromFile(context)))
                } else {
                    getListWordLiveData.postValue(ResponseData.success(result))
                }
            }, {
                getListWordLiveData.postValue(ResponseData.error("Something went wrong!", null))
            }).disposedBy(compositeDisposable)
    }

    fun searchWord(key: String, listAllWord: MutableList<WordResponse>): MutableList<WordResponse> {
        return listAllWord.filter { it.english.startsWith(key, ignoreCase = true) }.toMutableList()
    }

    fun deleteWord(wordEntity: ListWordEntity) {
        deleteWordLiveData.postValue(ResponseData.loading())
        repository.deteteOneWord(wordEntity)
        deleteWordLiveData.postValue(ResponseData.success(true))
    }

    fun addWord(wordEntity: ListWordEntity) {
        addWordLiveData.postValue(ResponseData.loading())
        repository.addOneWord(wordEntity)
        addWordLiveData.postValue(ResponseData.success(true))
    }

    private fun getWordsFromFile(context: Context): MutableList<WordResponse> {
        val inputStream = context.resources.openRawResource(R.raw.dictionaries)
        val reader = BufferedReader(InputStreamReader(inputStream))

        val lines = mutableListOf<String>()
        val linesResult = mutableListOf<WordResponse>()
        var line: String?

        // Đọc từng dòng từ file và thêm vào list
        while (reader.readLine().also { line = it } != null) {
            lines.add(line!!)
        }

        // Đóng reader
        reader.close()
        for (line in lines) {
            val (english, vietnamese) = line.split("\t")
            val word = WordResponse(english, vietnamese)
            repository.addOneWord(word.mapToEntity())
            linesResult.add(word)
        }


        return linesResult
    }

    private fun appendWordToFile(word: WordResponse, dictionaryFile: File) {
        try {
            val writer = BufferedWriter(FileWriter(dictionaryFile, true))
            writer.append("${word.english}\t${word.vietnamese}")
            writer.newLine()
            writer.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}