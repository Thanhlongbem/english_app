package com.uet.english.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.uet.english.data.entities.ListWordEntity
import io.reactivex.Single

@Dao
interface ListWordDao {
    @Query("SELECT * FROM listWordEntity ORDER BY english ASC")
    fun getAllWord(): Single<MutableList<ListWordEntity>>

    @Query("SELECT * FROM listWordEntity WHERE english LIKE :english")
    fun filterWord(english: String): Single<MutableList<ListWordEntity>>

    @Query("DELETE FROM listWordEntity")
    fun deleteAllWord()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOneWord(recentSearchEntity : ListWordEntity)

    @Delete
    fun deleteOneWord(recentSearchEntity: ListWordEntity)
}