package com.uet.english.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.uet.english.main.home.model.WordResponse


@Entity(tableName = "listWordEntity")
data class ListWordEntity (
    @PrimaryKey
    @ColumnInfo(name = "english")
    val english: String,
    @ColumnInfo(name = "vietnamese")
    val vietnamese: String
) {
    fun mapToModel(): WordResponse {
        return WordResponse(english, vietnamese)
    }
}