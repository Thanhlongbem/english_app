package com.uet.english

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.os.bundleOf
import com.uet.english.extensions.initNavGraph
import com.uet.english.extensions.navHostFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navHostFragment(R.id.nav_host_fragment)?.initNavGraph(
            navGraph = R.navigation.nav_uet_english,
            startDestination = R.id.homeFragment,
            bundle = bundleOf(

            )
        )
    }
}