package com.uet.english.core.rest

object Constants {
    const val DEFAULT_SERVER = "https://translate.googleapis.com/"
    var API_BASE_PATH = DEFAULT_SERVER


    const val CLIPBOARD = "CLIPBOARD"
}