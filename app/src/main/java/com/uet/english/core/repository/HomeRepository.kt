package com.uet.english.core.repository

import com.uet.english.data.entities.ListWordEntity
import com.uet.english.data.local.ListWordDao
import com.uet.english.extensions.execute
import com.uet.english.main.home.model.WordResponse
import io.reactivex.Single

class HomeRepository(private val dao: ListWordDao) {
    fun getAllWord(): Single<MutableList<ListWordEntity>> {
        return dao.getAllWord()
    }

    fun deteteOneWord(wordEntity: ListWordEntity) {
        Single.fromCallable { dao.deleteOneWord(wordEntity) }
            .execute()
    }

    fun deleteAllWord() {
        Single.fromCallable { dao.deleteAllWord() }
            .execute()
    }

    fun addWord(wordEntity: ListWordEntity): Single<Long> {
        return dao.getAllWord()
            .flatMap {
                return@flatMap getAllWord()
            }.flatMap {
                var isExist = false
                it.forEach {
                    if(it.english.lowercase() == wordEntity.english.lowercase()) {
                        isExist = true
                    }
                }

                if(!isExist) {
                    dao.insertOneWord(wordEntity)
                    return@flatMap Single.just(0L)
                } else {
                    return@flatMap Single.just(0L)
                }
            }
    }

    fun addOneWord(wordEntity: ListWordEntity) {
        dao.insertOneWord(wordEntity)
    }

    fun addAllWord(listWord: MutableList<WordResponse>): Single<Long> {
        dao.deleteAllWord()
        listWord.forEach{ word ->
            addWordWithoutGetAll(word.mapToEntity())
        }
        return Single.just(0L)
    }

    private fun addWordWithoutGetAll(wordEntity: ListWordEntity) {
        dao.insertOneWord(wordEntity)
    }
}