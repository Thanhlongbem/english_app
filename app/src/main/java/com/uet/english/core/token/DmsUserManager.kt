package com.uet.english.core.token

import android.text.TextUtils
import com.uet.english.main.login.model.UserInfo
import com.uet.english.main.login.model.UserToken
import com.uet.english.utils.HMPreference
import com.uet.english.utils.LogUtils
import com.google.gson.Gson

object DmsUserManager {

    private const val KEY_USER_INFO = "KEY_USER_INFO"
    private const val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"

    private var userInfoDisk: UserInfo? = null
    private var accessTokenDisk: String? = null

    val userInfo: UserInfo
        get() {
            if (userInfoDisk != null && userInfoDisk!!.userId.isNotEmpty()) {
                return userInfoDisk!!
            }
            val json = HMPreference.getString(KEY_USER_INFO)
            if (json.isNotEmpty()) {
                try {
                    val userCache = Gson().fromJson(json, UserInfo::class.java)
                    if (userCache != null && userCache.userId.isNotEmpty()) {
                        userInfoDisk = userCache
                        return userCache
                    }
                } catch (e: Exception) {
                    LogUtils.d("Could not parse")
                }
            }

            return UserInfo()
        }

    val accessToken: String?
        get() {
            if (accessTokenDisk == null) {
                accessTokenDisk = getUserToken()?.accessToken
            }
            return accessTokenDisk
        }

    fun isValidUserInfo(): Boolean{
        return userInfo.userId.isNotEmpty() && accessToken != null
    }

    fun updateUserInfo(userInfo: UserInfo) {
        val json = Gson().toJson(userInfo)
        HMPreference.setString(KEY_USER_INFO, json)
    }

    private fun getUserToken(): UserToken? {
        val value = HMPreference.getString(KEY_ACCESS_TOKEN)
        if (!TextUtils.isEmpty(value)) {
            try{
                val userToken = Gson().fromJson(value, UserToken::class.java)
                if (userToken != null && !TextUtils.isEmpty(userToken.accessToken)) {
                    return userToken
                }
            }catch (e: Exception){
                LogUtils.d("Could not parse")
            }
        }
        return null
    }

    fun setUserToken(userToken: UserToken) {
        if (!TextUtils.isEmpty(userToken.accessToken)) {
            HMPreference.setString(KEY_ACCESS_TOKEN, Gson().toJson(userToken))
        }
    }

    fun logout(){
        clearUserInfo()
        clearUserToken()
        userInfoDisk = null
        accessTokenDisk = null
    }

    private fun clearUserInfo() {
        HMPreference.setString(KEY_USER_INFO, "")
    }

    private fun clearUserToken() {
        HMPreference.setString(KEY_ACCESS_TOKEN, "")
    }

}